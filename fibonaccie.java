package everything;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		//List<BigInteger> list = getBig(100);
		//System.out.println(list);
		BigInteger el = getElement(1000);
		System.out.println(el);
	}
	
	public static List<BigInteger> getBig(int leng) {
		List<BigInteger> result = new ArrayList<BigInteger>();
		if(leng == 1) {
			result.add(BigInteger.ZERO);
			return result;
		} else if(leng == 2) {
			result.add(BigInteger.ZERO);
			result.add(BigInteger.ONE);
			return result;
		}
		
		result.add(BigInteger.ZERO);
		result.add(BigInteger.ONE);
		
		for(int i = 1; i < leng; i++) {
			BigInteger sum = result.get(i-1).add(result.get(i));
			result.add(sum);
		}
		
		return result;
	}
	
	public static BigInteger getElement(int index) {
		List<BigInteger> result = new ArrayList<BigInteger>();
		
		result.add(BigInteger.ZERO);
		result.add(BigInteger.ONE);
		
		for(int i = 1; i < index-1; i++) {
			BigInteger sum = result.get(i-1).add(result.get(i));
			result.add(sum);
		}
		
		return result.get(index-1);
	}
	
}
